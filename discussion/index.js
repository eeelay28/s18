// // Function able to receive data without the use of global variables or prompt()

// // "name" - is a parameter
// // A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
// function printName(name){
// 	console.log("My name is " + name);
// };

// // Data passed into a function invocation can be received by the function
// // This is what we call an argument
// printName("Jungkook");
// printName("Thonie");

// // Data passed into the function through funtion invocation is called arguments
// // The argument is then stored within a container called a parameter
// function printMyAge(age){
// 	console.log("I am " + age);
// };

// printMyAge(25);
// printMyAge();

// // check divisibility reusably using a function with arguments and parameter
// function checkDivisibilityBy8(num){
// 	let remainder = num % 8;
// 	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

// 	let isDivisibleBy8 = remainder === 0;
// 	console.log("Is " + num + " divisible by 8?");
// 	console.log(isDivisibleBy8);
// };

// checkDivisibilityBy8(64);
// checkDivisibilityBy8(27);
// /*
// 	Mini-Activity

// 	1. Create a function which is capable to receive data as an argument:
// 		- This function should be able to receive the name of your favorite superhero
// 		- Display the name of your favorite superhero in the console
// 	 2. Create a function which is capable to receive a number as an argument:
// 	 	- This function should be able to receive a any number
// 	 	- Display the number and state if even number
//  */

// function DisplayMyFavoriteSuperhero(favoritesuperhero){
//   console.log("My Favorite Superhero is " + favoritesuperhero);
// }

// DisplayMyFavoriteSuperhero("Flash");


// function DisplayNumber(number){

//   let rem=number %2;

//   let isNumberOdd = rem !== 0;
//   let isNumberEven = rem === 0;

//   console.log("Your number is " + number);
//   console.log("Is the Number "+ number+ " Odd? " + isNumberOdd);
//   console.log("Is the Number "+ number+ " Even? " + isNumberEven);
 
// }

// DisplayNumber(25);

// function myFavoriteSongs(songOne,songTwo,songThree,songFour,songFive){

//   console.log("My favorite Songs are: ");
//     console.log("* " + songOne);
//     console.log("* " + songTwo);
//     console.log("* " + songThree);
//     console.log("* " + songFour);
//     console.log("* " + songFive);

 
// }
// myFavoriteSongs("Terrified","Out of my League","How Deep is your Love","Tulad Mo", "Kanlungan");












// function AddTwoNumbers(numberOne,numberTwo){
//  let resultOfAddition = numberOne + numberTwo;
//  return resultOfAddition;
// }

//  AddTwoNumbers(2,3);
//  console.log(AddTwoNumbers(2,3));



// function ProductofTwoNumbers(productOne,productTwo){
//   return productOne*productTwo;
// }

// let product = ProductofTwoNumbers(2,5);
// console.log("Result: " + product);




function getTheAreaofCircle(piValue,radiusValue){
  return (piValue*(radiusValue**2));
}

let circleArea = getTheAreaofCircle(3.14,5);
console.log(circleArea);

/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

  function getPercentageOfScore(score,total){
    let getPercentageResult = ((score/total)*100);

    if (getPercentageResult > 75 ){
      let isPassed = getPercentageResult > 75;
      return isPassed;
    };
  }
  
  let isPassingScore = getPercentageOfScore(8,10);
  console.log("Result: " + isPassingScore);